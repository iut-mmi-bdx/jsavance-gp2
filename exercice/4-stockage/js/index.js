// sessionStorage
//sessionStorage.setItem('name', 'Charline')
const name = sessionStorage.getItem('name')

// localStorage
localStorage.setItem('name', 'Charles')
const nameLocal = localStorage.getItem('name')